OOB_MSGTYPE_CHK_CLEAR_TARGETS = "tfd.checkcleartargets";
OOB_MSGTYPE_SET_TOKEN_SELECTION = "tfd.tokenselection";

function onInit()
    OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_CHK_CLEAR_TARGETS, onOOBCheckClearTargets);
    OOBManager.registerOOBMsgHandler(OOB_MSGTYPE_SET_TOKEN_SELECTION, onOOBSetTokenSelection);

    CombatManager.setCustomCombatReset(onCombatReset);
    CombatManager.setCustomRoundStart(onRoundStart);
    CombatManager.setCustomTurnStart(onTurnStart);
    CombatManager.setCustomTurnEnd(onTurnEnd);

    OptionsManager.registerOption2("TFD_CTE", true, "option_header_client", "option_label_TFD_CTE", "option_entry_cycler",
            { labels = "option_val_on", values = "on", baselabel = "option_val_off", baseval = "off", default = "on" });
end

function onCombatReset()
    -- clear all targets, regardless of local setting
    for _,v in pairs(CombatManager.getCombatantNodes()) do
        TargetingManager.clearCTTargets(v);
    end
end

function onOOBCheckClearTargets(msgOOB)
    local nodeActive = CombatManager.getCTFromNode(msgOOB.sActorPath);

    if DB.isOwner(nodeActive) and OptionsManager.isOption("TFD_CTE", "on") then
        TargetingManager.clearCTTargets(nodeActive);
    end
end

function onOOBSetTokenSelection(msgOOB)
    self.toggleSelectedToken(msgOOB.bSelect == "1");
end

function onRoundStart()
    -- send OOB to everyone to clear targets, respecting their local setting
    for _,v in pairs(CombatManager.getCombatantNodes()) do
        self.sendCheckClearTargets(v.getPath(), DB.getOwner(v));
    end
end

function onTurnStart(nodeEntry)
    if not nodeEntry then
        return;
    end

    local _, sRecord = DB.getValue(nodeEntry, "link");
    local sOwner = DB.getOwner(sRecord);

    self.sendSetTokenSelection("1", sOwner);
end

function onTurnEnd(nodeEntry)
    if not nodeEntry then
        return;
    end

    local _, sRecord = DB.getValue(nodeEntry, "link");
    local sOwner = DB.getOwner(sRecord);

    self.sendCheckClearTargets(nodeEntry.getPath(), sOwner);
    self.sendSetTokenSelection("0", sOwner);
end

function sendCheckClearTargets(sActorPath, sRecipient)
    local message = {};
    message.type = OOB_MSGTYPE_CHK_CLEAR_TARGETS;
    message.sActorPath = sActorPath;
    message.user = User.getUsername();

    self.sendOOBToActiveOrHost(message, sRecipient);
end

-- send an OOB to the recipient, or the host if the recipient is not active
function sendOOBToActiveOrHost(tMessage, sRecipient)
    local bUserIsActive = false;

    for _,vUser in ipairs(User.getActiveUsers()) do
        if vUser == sRecipient then
            bUserIsActive = true;
            break;
        end
    end

    if bUserIsActive then
        Comm.deliverOOBMessage(tMessage, sRecipient);
    else
        Comm.deliverOOBMessage(tMessage);
    end
end

function sendSetTokenSelection(bSelect, sRecipient)
    local message = {};
    message.type = OOB_MSGTYPE_SET_TOKEN_SELECTION;
    message.bSelect = bSelect;
    message.user = User.getUsername();

    self.sendOOBToActiveOrHost(message, sRecipient);
end

function toggleSelectedToken(bSelect)
    local nodeActive = CombatManager.getActiveCT();
    local nodeCT = CombatManager.getTokenFromCT(nodeActive);
    local container = ImageManager.getImageControl(nodeCT, false);

    if nodeCT and container then
        container.clearSelectedTokens();
        container.selectToken(nodeCT, bSelect);
    end
end
