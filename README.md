# FGU Targeting for Dummies

This extension attempts to simplify the targeting process by making it consistent and repeatable for players that don't tend to use the combat tracker.

## Features

### Automatic Selection

* At turn start, automatically select the active token, if you're the owner

### Automatic Target Clearing

* Client and host targets are cleared when the combat tracker is reset
* Adds a local option, "Clear at turn end", which clears a token's targets when the turn or round advances
